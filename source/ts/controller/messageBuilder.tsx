import _IMessageText from "~/data/config.default";

interface IState {
	carNumber: string
}

export  function validNumber(_this, carNumber:string): void {
	 const reg = /^[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}$/;
	 const n = reg.test(carNumber);
	 if(n) {
	 	_this.setState({ numberValid: true });
	 	messageBuilder(_this, "error", _IMessageText.succes); 
	 }
	 else {
	 	_this.setState({ numberValid: false });
	 	messageBuilder(_this, "data", _IMessageText.invalidValidation);
	 }
}

export function messageBuilder(_this, type: string , data: string){
	_this.setState({
		messageData: {
			typeMasage  : type,
			error 		: data
		}
	})
}
