import * as React from "react";
import { emitter } from "../../../controller/messageEmmiter";
import _IMessageText from "~/data/config.default";
import { validNumber, messageBuilder } from "../../../controller/messageBuilder"

interface IState {
	value : string,
	numberValid : boolean,
	data?: object[] | null,
	messageData?: object
}

export class Header extends React.Component<{}, IState> {
	constructor(props:IState) {
		super(props);
		this.state = {
			value: "" ,
			numberValid : false,
			data : null
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	private onKeyPressed(e) {
		if( this.state.value.length === 8 ) {
			validNumber(this, this.state.value)
			if(e.key === "Enter") this.handleSubmit(e)
		}
		else{
		  this.setState({ numberValid: false });
		  messageBuilder(this, "error", _IMessageText.notEnoughChar)
		}
	}

	public handleChange(e) {
		this.setState({value: e.target.value});
	}

	public handleSubmit(e) {
		e.preventDefault();
		console.log(this.state.numberValid, "this.state.numberValid")
		if(!this.state.numberValid) return
		fetch("http://localhost:8080/api/v1/car-info/" + this.state.value)
		.then((data) => data.json()).then((data) => { 
			this.setState({ data: data.result }) 
			emitter.emit("messageData", this.state.data);
		})
		.catch((err: Error) => messageBuilder(this, "error", _IMessageText.invalidValidation));
	}

    public componentDidUpdate(){
    	emitter.emit("messageData", this.state.messageData);
    }

	public render(): JSX.Element {
		return (
			<header>
				<form onSubmit={this.handleSubmit}>
					<input type="text" value={this.state.value} onKeyUp={(e) => this.onKeyPressed(e)} onChange={this.handleChange} />
					{this.state.numberValid ? <input type="submit" value="Submit" /> : null}
				</form>
			</header>
		);
	}
}
