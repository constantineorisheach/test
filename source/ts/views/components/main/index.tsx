import * as React from "react";
import { emitter } from "../../../controller/messageEmmiter";

interface IState {
    data?:  {
    	owner?: string;
    	year ?: string;
    	crashesCount ?: string;
    	ownersCount ?: string;
    	data?: string;
    	typeMasage?: string;
    	error?: string;
    } | null
}

export  class Main extends React.Component<{}, IState> {
	public state: IState = {
        data: null
	}

    public render(): JSX.Element | null {
    	if(!this.state.data) return <h1>Введите данные в поле</h1>;
		return (
			<main>
				{
					this.state.data.typeMasage === "error" ?
					this.messageError() :
					this.messageData()
				}
			</main>
		);
	}

	public componentWillMount(): void {
		this.init();
	}

	public messageError(): JSX.Element | null {
		if(!this.state.data) return null;
		return <h1>{this.state.data.error}</h1>
	}

	public messageData(): JSX.Element | null {
		if(!this.state.data) return  null;
    	const {owner, year, crashesCount, ownersCount, data} = this.state.data;
		return(
				<ul> 
					<li><span>Владелец</span> <h1>{owner}</h1></li>
					<li><span>Год</span><h1>{year}</h1></li>
					<li><span>Количество ДТП</span><h1>{crashesCount}</h1></li>
					<li><span>Количество владельцов</span><h1>{ownersCount}</h1></li>
				</ul>
		)
	}

	public init(){
		emitter.on("messageData",
		(data: object) => {
			this.setState({ data });
		});
	}

}