export interface IMessageText {
	notEnoughChar       : string,
	invalidValidation   : string,
	succes: string,
	welcomeText: string
}
export default {
		notEnoughChar       : "Номер должен состоять из 8 символов",
		invalidValidation   : "Введенный номер несоответствует формату",
		welcomeText 	    : "Для получения информации по номеру авто, введите данные в поле ввода",
		succes				: "Успех введенный номер соответствует формату. нажмите подтвердить или ЕНТЕР"
} as IMessageText;
